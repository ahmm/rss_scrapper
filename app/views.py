from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from app.models import ExchangeRate, Currency
from app.serializers import ExchangeRateSerializer, CurrencySerializer


class ExchangeRatesListView(generics.ListAPIView):
    serializer_class = ExchangeRateSerializer

    def get_queryset(self):
        currency = self.kwargs.get('currency')
        return ExchangeRate.objects.filter(target_currency__short_name=currency) or ExchangeRate.objects.all()


class ExchangeRatesLatestView(generics.RetrieveAPIView):
    serializer_class = ExchangeRateSerializer

    def get_object(self):
        currency = self.kwargs.get('currency')
        return ExchangeRate.objects.filter(target_currency__short_name=currency).latest('date')



