import re

from bs4 import BeautifulSoup
from celery import shared_task
import requests
import feedparser

from app.models import Currency, ExchangeRate

@shared_task
def get_exchange_rates_from_rss():
    BASE_URL = 'https://www.ecb.europa.eu'
    INDEX_URL = f'{BASE_URL}/home/html/rss.en.html'
    response = requests.get(INDEX_URL)

    try:
        soup = BeautifulSoup(response.content, 'html.parser')
        http_elements = soup.find_all(href=re.compile('^/rss/fxref'))
        relative_links = [link.attrs['href'] for link in http_elements]
        rss_links = [f'{BASE_URL}{link}' for link in relative_links]

        for feed_link in rss_links:
            try:
                feed = feedparser.parse(feed_link)
                latest_feed = feed['entries'][0]

                rate, base_currency = latest_feed['cb_exchangerate'].split('\n')
                data = {
                    'target_currency': latest_feed['cb_targetcurrency'],
                    'rate': rate,
                    'base_currency': base_currency,
                    'date': latest_feed['updated']
                }

                target_currency, _ = Currency.objects.get_or_create(short_name=data['target_currency'])
                base_currency, _ = Currency.objects.get_or_create(short_name=data['base_currency'])
                rate, _ = ExchangeRate.objects.get_or_create(target_currency=target_currency, base_currency=base_currency, rate=data['rate'], date=data['date'])
                print(rate)

            except Exception as e:
                # TODO: change prints to logging
                print(r'Failed to parse rss feed from {feed_link}')
                print(e)
    except Exception as e:
        print(f'Failed to obtain rss feeds from {INDEX_URL}')
