from django.urls import path

from app.views import ExchangeRatesListView, ExchangeRatesLatestView

urlpatterns = [
    path('rates/', ExchangeRatesListView.as_view()),
    path('rates/<currency>/', ExchangeRatesListView.as_view()),
    path('rates/<currency>/latest', ExchangeRatesLatestView.as_view()),
]
