from django.db import models


class Currency(models.Model):
    short_name = models.CharField(max_length=3)

    def __str__(self):
        return self.short_name


class ExchangeRate(models.Model):
    """
    Model representing a single update in exchange rates between currencies.
    """
    base_currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name='+')
    target_currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name='+')
    rate = models.DecimalField(decimal_places=4, max_digits=15)
    date = models.DateTimeField()

    class Meta:
        ordering = ['-date', 'target_currency']

    def __str__(self):
        return f'{self.target_currency}/{self.base_currency} {self.rate} at {self.date}'
