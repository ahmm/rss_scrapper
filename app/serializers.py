from rest_framework import serializers

from app.models import ExchangeRate, Currency


class CurrencySerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='short_name')

    class Meta:
        model = Currency
        fields = ('name', )


class ExchangeRateSerializer(serializers.ModelSerializer):
    currency = serializers.StringRelatedField(source='target_currency')
    base_currency = serializers.StringRelatedField()

    class Meta:
        model = ExchangeRate
        fields = ('currency', 'base_currency', 'rate', 'date')


